<?php
/**
 * @file
 * Demo implementation of template converter.
 */

$args = array(
  // You to edit this to point to the right location ==>
  "template_url" => "file:///var/projects/office/template.odt",
  "replace" => array(
    "example1" => "Substituted value 1",
    "example2" => "this is cool huh!?",
    "value1" => "tables work too",
    "value2" => "awesomeness included!",
  ),
);

// Execute the python script with the JSON data.
// You'll need to replace this with an absolute path if you move oofice.py
// outside your document root.
$result = shell_exec('python ooffice.py ' . escapeshellarg(json_encode($args)));

header("Pragma: public");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Cache-Control: public");
header("Content-Description: File Transfer");
header("Content-Type: application/pdf");
header("Content-Transfer-Encoding: binary");
echo $result;
